name := "ocelot-desktop"
version := "0.1"
scalaVersion := "2.13.2"

lazy val root = project.in(file("."))
  .dependsOn(brain % "compile->compile")
  .aggregate(brain)

lazy val brain = ProjectRef(file("lib/ocelot-brain"), "ocelot-brain")

libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.11.1"
libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.11.1"


val lwjglVersion = "2.9.3"

libraryDependencies += "org.lwjgl.lwjgl" % "lwjgl" % lwjglVersion
libraryDependencies += "org.lwjgl.lwjgl" % "lwjgl-platform" % lwjglVersion classifier "natives-linux"
libraryDependencies += "org.lwjgl.lwjgl" % "lwjgl-platform" % lwjglVersion classifier "natives-windows"
libraryDependencies += "org.lwjgl.lwjgl" % "lwjgl-platform" % lwjglVersion classifier "natives-osx"

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}