package ocelot.desktop.graphics.mesh

import ocelot.desktop.graphics.ShaderProgram
import ocelot.desktop.graphics.buffer.{IndexBuffer, VertexBuffer}
import ocelot.desktop.util.{Logging, Resource, ResourceManager}
import org.lwjgl.opengl._

class VertexArray(shader: ShaderProgram) extends Logging with Resource {
  val array: Int = GL30.glGenVertexArrays()

  ResourceManager.registerResource(this)

  def freeResource(): Unit = {
    logger.debug(s"Destroyed VAO (ID: $array)")
    GL30.glDeleteVertexArrays(array)
  }

  def addVertexBuffer[V <: Vertex](vbuf: VertexBuffer[V], instanced: Boolean = false): Unit = {
    bind()

    for (attr <- vbuf.ty.attributes) {
      val location = shader.getAttributeLocation(attr.name)

      GL20.glEnableVertexAttribArray(location)
      vbuf.bind()

      GL20.glVertexAttribPointer(location, attr.size, attr.ty, attr.normalized, attr.stride, attr.pointer)

      if (instanced) ARBInstancedArrays.glVertexAttribDivisorARB(location, 1)
    }
  }

  def addIndexBuffer(ibuf: IndexBuffer): Unit = {
    bind()
    ibuf.bind()
  }

  def bind(): Unit = GL30.glBindVertexArray(array)
}