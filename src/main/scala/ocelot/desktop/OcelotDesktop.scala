package ocelot.desktop

import java.io.{PrintWriter, StringWriter}

import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.widget.RootWidget
import ocelot.desktop.util._
import org.apache.logging.log4j.LogManager
import totoro.ocelot.brain.Ocelot
import totoro.ocelot.brain.event._
import totoro.ocelot.brain.workspace.Workspace

import scala.io.Source


object OcelotDesktop extends Logging {
  var cl_args: Array[String] = _
  var root: RootWidget = _
  val tpsCounter = new FPSCalculator

  def mainInner(): Unit = {
    logger.info("Starting up Ocelot Desktop")

    Ocelot.initialize(LogManager.getLogger(Ocelot))
    ColorScheme.load(Source.fromURL(getClass.getResource("/ocelot/desktop/colorscheme.txt")))
    createWorkspace()

    root = new RootWidget()

    UiHandler.init(root)

    setupEventHandlers()

    new Thread(() => {
      val ticker = new Ticker
      ticker.tickInterval = 50
      while (true) {
        workspace.update()
        tpsCounter.tick()
        ticker.waitNext()
      }
    }).start()

    UiHandler.start()

    logger.info("Cleaning up")

    ResourceManager.freeResources()
    UiHandler.terminate()

    logger.info("Thanks for using Ocelot Desktop")
    System.exit(0)
  }

  def main(args: Array[String]): Unit = {
    cl_args = args
    try mainInner()
    catch {
      case e: Exception =>
        val sw = new StringWriter
        val pw = new PrintWriter(sw)

        e.printStackTrace(pw)

        logger.error(s"${sw.toString}")
        System.exit(1)
    }
  }

  var workspace: Workspace = _

  private def createWorkspace(): Unit = {
    workspace = new Workspace()
  }

  private def setupEventHandlers(): Unit = {
    EventBus.listenTo(classOf[BeepEvent], { case event: BeepEvent =>
      Audio.beep(event.frequency, event.duration)
    })

    EventBus.listenTo(classOf[BeepPatternEvent], { case event: BeepPatternEvent =>
      logger.info(s"[EVENT] Beep (address = ${event.address}, pattern = ${event.pattern})")
    })

    EventBus.listenTo(classOf[MachineCrashEvent], { case event: MachineCrashEvent =>
      logger.info(s"[EVENT] Machine crash! (address = ${event.address}, ${event.message})")
    })
  }
}