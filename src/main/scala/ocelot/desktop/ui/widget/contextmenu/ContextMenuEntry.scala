package ocelot.desktop.ui.widget.contextmenu

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.event.handlers.{ClickHandler, HoverHandler}
import ocelot.desktop.ui.event.{ClickEvent, HoverEvent, MouseEvent}
import ocelot.desktop.ui.widget.{Label, PaddingBox, Widget}
import ocelot.desktop.util.animation.ValueAnimation
import ocelot.desktop.util.animation.easing.{EaseInQuad, EaseOutQuad}

class ContextMenuEntry(label: String, onClick: () => Unit = () => {}) extends Widget with ClickHandler with HoverHandler {
  private[contextmenu] val alpha = new ValueAnimation(0f, 10f)
  private[contextmenu] val textAlpha = new ValueAnimation(0f, 5f)
  private[contextmenu] val trans = new ValueAnimation(0f, 20f)
  private[contextmenu] var contextMenus: ContextMenus = _
  private[contextmenu] var contextMenu: ContextMenu = _
  private[contextmenu] var isGhost: Boolean = false

  children :+= new PaddingBox(new Label {
    override def text: String = label
    override def color: Color = ColorScheme("ContextMenuText")
  }, Padding2D(left = 12f, right = 16f, top = 5f, bottom = 5f))

  override def receiveMouseEvents: Boolean = !isGhost

  eventHandlers += {
    case ClickEvent(MouseEvent.Button.Left, _) if !contextMenu.isOpening => clicked()
    case HoverEvent(HoverEvent.State.Enter) => enter()
    case HoverEvent(HoverEvent.State.Leave) if !isGhost => leave()
  }

  override def minimumSize: Size2D = layout.minimumSize.max(Size2D(150, 1))

  protected def clicked(): Unit = {
    onClick()
    contextMenus.closeAll()
    contextMenus.setGhost(this)

    isGhost = true
    alpha.goto(0f)
    textAlpha.goto(0f)
    alpha.speed = 2.5f
    textAlpha.speed = 2.5f
    trans.speed = 0f
  }

  protected def enter(): Unit = {
    alpha.speed = 10f
    alpha.goto(1f)
    trans.easing = EaseInQuad
    trans.goto(2f)
  }

  protected def leave(): Unit = {
    alpha.speed = 1f
    alpha.goto(0f)
    trans.easing = EaseOutQuad
    trans.goto(0f)
  }

  override def draw(g: Graphics): Unit = {
    alpha.update()
    textAlpha.update()
    trans.update()

    g.rect(bounds.mapW(_ - 8).mapX(_ + 4), ColorScheme("ContextMenuHover").withAlpha(alpha.value))

    g.save()
    g.translate(trans.value, 0f)
    g.alphaMultiplier = textAlpha.value
    drawChildren(g)
    g.restore()
  }
}
