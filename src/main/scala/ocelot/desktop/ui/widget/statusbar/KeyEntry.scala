package ocelot.desktop.ui.widget.statusbar

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.widget.Widget
import ocelot.desktop.util.DrawUtils

class KeyEntry(val key: String, val text: String) extends Widget {
  override def minimumSize: Size2D = Size2D(key.length * 8 + 24 + text.length * 8, 16)
  override def maximumSize: Size2D = minimumSize

  override def draw(g: Graphics): Unit = {
    g.background = Color.Transparent
    g.foreground = ColorScheme("StatusBarKey")
    g.setSmallFont()
    DrawUtils.borderedText(g, position.x + 8, position.y + 5, key)

    g.foreground = ColorScheme("StatusBarForeground")
    g.setNormalFont()
    g.text(position.x + key.length * 8 + 16, position.y, text)
  }
}
