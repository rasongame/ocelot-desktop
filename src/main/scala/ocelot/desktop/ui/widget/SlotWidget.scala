package ocelot.desktop.ui.widget
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.Graphics

class SlotWidget extends Widget {
  def icon: String = "Empty"

  def tierIcon: String = null

  override def minimumSize: Size2D = Size2D(36, 36)
  override def maximumSize: Size2D = minimumSize

  override def draw(g: Graphics): Unit = {
    g.sprite("EmptySlot", bounds)
    if (tierIcon != null)
      g.sprite(tierIcon, bounds.inflate(-2))
    g.sprite(icon, bounds.inflate(-2))
  }
}
