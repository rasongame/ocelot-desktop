package ocelot.desktop.ui.widget

import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.layout.PaddingLayout

class PaddingBox(inner: Widget, padding: Padding2D) extends Widget {
  override protected val layout = new PaddingLayout(this, padding)

  children = Array(inner)
}
