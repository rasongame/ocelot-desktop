package ocelot.desktop.ui.widget

import ocelot.desktop.color.RGBAColor
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.UiHandler.graphics
import ocelot.desktop.ui.event.KeyEvent
import ocelot.desktop.ui.layout.{CopyLayout, LinearLayout}
import ocelot.desktop.ui.widget.contextmenu.ContextMenus
import ocelot.desktop.ui.widget.statusbar.StatusBar
import ocelot.desktop.util.{DrawUtils, Orientation}
import org.lwjgl.input.Keyboard

class RootWidget extends Widget {
  override protected val layout = new LinearLayout(this, orientation = Orientation.Vertical)
  root = Some(this)

  val workspaceView = new WorkspaceView
  workspaceView.root = root
  workspaceView.createDefaultWorkspace()

  val contextMenus = new ContextMenus
  val statusBar = new StatusBar

  children :+= new Widget {
    override protected val layout = new CopyLayout(this)
    children :+= workspaceView
    children :+= contextMenus
  }

  children :+= statusBar

  private var isDebugViewVisible = false
  private var isFPSVisible = false

  eventHandlers += {
    case KeyEvent(KeyEvent.State.Release, Keyboard.KEY_F1, _) =>
      isDebugViewVisible = !isDebugViewVisible
    case KeyEvent(KeyEvent.State.Release, Keyboard.KEY_F2, _) =>
      isFPSVisible = !isFPSVisible
  }

  override def draw(g: Graphics): Unit = {
    super.draw(g)

    if (isDebugViewVisible) drawDebugView(g)
  }

  private def drawDebugView(g: Graphics): Unit = {
    for (widget <- UiHandler.getHierarchy.reverseIterator) {
      if (widget != null) {
        val b = widget.clippedBounds
        if (b.w > 1f && b.h > 1f) {
          DrawUtils.ring(graphics, b.x, b.y, b.w, b.h, 1, RGBAColor(255, 0, 0, 50))
        }
      }
    }
  }

  override def update(): Unit = {
    super.update()

    if (isFPSVisible)
      UiHandler.windowTitle = s"Ocelot Desktop [FPS: ${UiHandler.fps}]"
    else
      UiHandler.windowTitle = "Ocelot Desktop"
  }
}
