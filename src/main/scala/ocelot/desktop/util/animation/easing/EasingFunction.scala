package ocelot.desktop.util.animation.easing

trait EasingFunction extends Function[Float, Float] {
  def derivative(t: Float): Float
}
