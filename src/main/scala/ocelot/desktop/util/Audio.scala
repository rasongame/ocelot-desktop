package ocelot.desktop.util

import java.nio.ByteBuffer

import org.lwjgl.openal.AL10

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Audio extends Logging {
  private val sampleRate: Int = 44100
  private val amplitude: Int = 32

  private val sources = new mutable.HashMap[Int, Int]()
  private val scheduled = new ArrayBuffer[(Short, Short)]()

  def beep(frequency: Short, duration: Short): Unit = {
    scheduled += ((frequency, duration))
  }

  private def _beep(frequency: Short, duration: Short): Unit = {
    val source = AL10.alGenSources()
    AL10.alSourcef(source, AL10.AL_PITCH, 1)
    AL10.alSourcef(source, AL10.AL_GAIN, 0.3f)
    AL10.alSource3f(source, AL10.AL_POSITION, 0, 0, 0)
    AL10.alSourcei(source, AL10.AL_LOOPING, AL10.AL_FALSE)

    val samples = duration * sampleRate / 1000
    val data = ByteBuffer.allocateDirect(samples)
    val step = frequency / sampleRate.toFloat
    var offset = 1f

    for (_ <- 0 until samples) {
      val angle = 2 * math.Pi * offset
      val value = (math.signum(math.sin(angle)) * amplitude).toByte ^ 0x80

      offset += step
      if (offset > 1) offset -= 1

      data.put(value.toByte)
    }

    data.rewind()

    val buffer = AL10.alGenBuffers()
    AL10.alBufferData(buffer, AL10.AL_FORMAT_MONO8, data, sampleRate)

    AL10.alSourceQueueBuffers(source, buffer)
    AL10.alSourcePlay(source)

    sources += (source -> buffer)
  }

  def update(): Unit = {
    for ((frequency, duration) <- scheduled)
      _beep(frequency, duration)

    scheduled.clear()

    sources.filterInPlace((source, buffer) => {
      AL10.alGetSourcei(source, AL10.AL_SOURCE_STATE) == AL10.AL_PLAYING || {
        AL10.alDeleteSources(source)
        AL10.alDeleteBuffers(buffer)
        false
      }
    })
  }
}
