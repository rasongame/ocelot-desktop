package ocelot.desktop.util

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.{Color, RGBAColorNorm}

object TierColor {
  val Tier0: RGBAColorNorm = ColorScheme("Tier0")
  val Tier1: RGBAColorNorm = ColorScheme("Tier1")
  val Tier2: RGBAColorNorm = ColorScheme("Tier2")
  val Tier3: RGBAColorNorm = ColorScheme("Tier3")

  val Tiers: Array[RGBAColorNorm] = Array(Tier0, Tier1, Tier2, Tier3)

  def get(tier: Int): RGBAColorNorm = if (tier >= 0 && tier <= 3) Tiers(tier.min(3)) else Color.White
}
