package ocelot.desktop.node.nodes

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.color.Color
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.node.Node
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry, ContextMenuSubmenu}
import ocelot.desktop.util.TierColor
import totoro.ocelot.brain.entity.machine.Arguments
import totoro.ocelot.brain.entity.traits.Computer
import totoro.ocelot.brain.entity.{CPU, Case, EEPROM, GraphicsCard, HDDManaged, InternetCard, Memory, NetworkCard}
import totoro.ocelot.brain.loot.Loot
import totoro.ocelot.brain.util.Tier

class ComputerNode(val computer: Case, var disk_uuid: String = None.toString) extends Node {
  OcelotDesktop.workspace.add(computer)
  private var cpu = new CPU(Tier.Three)
  private var gpu = new GraphicsCard(Tier.Three)
  computer.add(cpu)
  computer.add(gpu)
  computer.add(new InternetCard)
  computer.add(new Memory(Tier.Six))
  computer.add(new Memory(Tier.Six))
  computer.add(new NetworkCard)

  if (disk_uuid.equals(None.toString)) {
    disk_uuid = java.util.UUID.randomUUID().toString
  }
  computer.add(new HDDManaged(disk_uuid, Tier.Three, disk_uuid.subSequence(0,5).toString))

  private val eeprom = Loot.OpenOsEEPROM.create()
  eeprom.asInstanceOf[EEPROM].readonly = false

  computer.add(eeprom)
  computer.add(Loot.OpenOsFloppy.create())

  override def environment: Computer = computer

  override val icon: String = "nodes/Computer"
  override def iconColor: Color = TierColor.get(computer.tier)

  override def setupContextMenu(menu: ContextMenu): Unit = {
    if (computer.machine.isRunning) {
      menu.addEntry(new ContextMenuEntry("Turn off", () => computer.turnOff()))
      menu.addEntry(new ContextMenuEntry("Reboot", () => {
        computer.turnOff()
        computer.turnOn()
      }))
    } else
      menu.addEntry(new ContextMenuEntry("Turn on", () => computer.turnOn()))

    menu.addEntry(new ContextMenuSubmenu("Set tier") {
      addEntry(new ContextMenuEntry("Tier 1", () => changeTier(Tier.One)))
      addEntry(new ContextMenuEntry("Tier 2", () => changeTier(Tier.Two)))
      addEntry(new ContextMenuEntry("Tier 3", () => changeTier(Tier.Three)))
      addEntry(new ContextMenuEntry("Tier 4 (Creative)", () => changeTier(Tier.Four)))
    })
    menu.addEntry(new ContextMenuSubmenu("Set CPU Tier") {
      addEntry(new ContextMenuEntry("Tier 1", () => cpu = new CPU(Tier.One)))
      addEntry(new ContextMenuEntry("Tier 2", () => cpu = new CPU(Tier.Two)))
      addEntry(new ContextMenuEntry("Tier 3", () => cpu = new CPU(Tier.Three)))
    })
    menu.addEntry(new ContextMenuSubmenu("Set GPU Tier") {
      addEntry(new ContextMenuEntry("Tier 1", () => gpu = new GraphicsCard(Tier.One)))
      addEntry(new ContextMenuEntry("Tier 2", () => gpu = new GraphicsCard(Tier.Two)))
      addEntry(new ContextMenuEntry("Tier 3", () => gpu = new GraphicsCard(Tier.Three)))
    })

    menu.addSeparator()
    super.setupContextMenu(menu)
  }

  private def changeTier(n: Int): Unit = {
    computer.tier = n
    currentWindow = new ComputerWindow(computer)
  }

  override def draw(g: Graphics): Unit = {
    super.draw(g)

    val isRunning = computer.machine.isRunning
    val hasErrored = computer.machine.lastError != null

    if (isRunning && !hasErrored)
      g.sprite("nodes/ComputerOnOverlay", position.x + 2, position.y + 2, size.width - 4, size.height - 4)

    if (!isRunning && hasErrored)
      g.sprite("nodes/ComputerErrorOverlay", position.x + 2, position.y + 2, size.width - 4, size.height - 4)

    if (isRunning && Math.random() > 0.7)
      g.sprite("nodes/ComputerActivityOverlay", position.x + 2, position.y + 2, size.width - 4, size.height - 4)
  }

  private var currentWindow: ComputerWindow = _

  override def window: Option[ComputerWindow] = {
    if (currentWindow == null) {
      currentWindow = new ComputerWindow(computer)
      Some(currentWindow)
    } else Some(currentWindow)
  }
}
