package ocelot.desktop.node.nodes

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.{IntColor, RGBAColor}
import ocelot.desktop.geometry.{Rect2D, Size2D, Vector2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.event.sources.MouseEvents
import ocelot.desktop.ui.event.{KeyEvent, MouseEvent, ScrollEvent}
import ocelot.desktop.ui.widget.window.BasicWindow
import ocelot.desktop.util.{DrawUtils, Logging}
import org.apache.commons.lang3.StringUtils
import org.lwjgl.input.Keyboard
import totoro.ocelot.brain.entity.Screen
import totoro.ocelot.brain.user.User
import totoro.ocelot.brain.util.PackedColor

class ScreenWindow(screenNode: ScreenNode) extends BasicWindow with Logging {
  private val fontWidth = 8f
  private val fontHeight = 16f
  private var lastMousePos = Vector2D(0, 0)
  private var sentTouchEvent = false

  private def screen: Screen = screenNode.screen

  private def screenWidth: Int = screen.getWidth

  private def screenHeight: Int = screen.getHeight

  override def minimumSize: Size2D = Size2D(screenWidth * fontWidth + 32, screenHeight * fontHeight + 36)

  override def receiveScrollEvents: Boolean = true

  eventHandlers += {
    case event: KeyEvent if isFocused =>
      event.state match {
        case KeyEvent.State.Press | KeyEvent.State.Repeat  =>
          screen.keyDown(event.char, event.code, User("myself"))

          // note: in opencomputers, key_down signal is fired __before__ clipboard signal
          if (event.code == Keyboard.KEY_INSERT)
            screen.clipboard(UiHandler.clipboard, User("myself"))
        case KeyEvent.State.Release =>
          screen.keyUp(event.char, event.code, User("myself"))
      }

    case event: MouseEvent if isFocused =>
      val pos = convertMousePos(UiHandler.mousePosition)
      val inside = checkBounds(pos)

      if (inside)
        lastMousePos = pos

      event.state match {
        case MouseEvent.State.Press if inside =>
          screen.mouseDown(pos.x, pos.y, event.button.id, User("myself"))
          sentTouchEvent = true

        case MouseEvent.State.Release =>
          if (event.button == MouseEvent.Button.Middle && inside)
            screen.clipboard(UiHandler.clipboard, User("myself"))

          if (sentTouchEvent) {
            screen.mouseUp(lastMousePos.x, lastMousePos.y, event.button.id, User("myself"))
            sentTouchEvent = false
          } else if (closeButtonBounds.contains(UiHandler.mousePosition)) {
            hide()
          }

        case _ =>
      }

    case event: ScrollEvent if isFocused =>
      screen.mouseScroll(lastMousePos.x, lastMousePos.y, event.offset, User("myself"))
  }

  private def checkBounds(p: Vector2D): Boolean = p.x >= 0 && p.y >= 0 && p.x < screenWidth && p.y < screenHeight

  private def convertMousePos(p: Vector2D): Vector2D = {
    Vector2D(
      math.floor((p.x - 16f - position.x) / fontWidth),
      math.floor((p.y - 16f - position.y) / fontHeight)
    )
  }

  override protected def dragRegions: Iterator[Rect2D] = Iterator(
    Rect2D(position.x, position.y, size.width, 20),
    Rect2D(position.x, position.y + size.height - 16, size.width, 16),
    Rect2D(position.x, position.y, 16, size.height),
    Rect2D(position.x + size.width - 16, position.y, 16, size.height),
  )

  override def update(): Unit = {
    super.update()

    val currentMousePos = convertMousePos(UiHandler.mousePosition)
    if (!checkBounds(currentMousePos) || currentMousePos == lastMousePos) return

    lastMousePos = currentMousePos

    if (isFocused)
      for (button <- MouseEvents.pressedButtons) {
        screen.mouseDrag(lastMousePos.x, lastMousePos.y, button.id, User("myself"))
      }
  }

  private def closeButtonBounds: Rect2D = Rect2D(
    position.x + fontWidth * screenWidth + 2,
    position.y - 2, 22, 22)

  override def draw(g: Graphics): Unit = {
    beginDraw(g)

    val sx = position.x + 16
    val sy = position.y + 20
    val w = fontWidth * screenWidth
    val h = fontHeight * screenHeight

    DrawUtils.shadow(g, sx - 22, sy - 22, w + 44, h + 52, 0.5f)
    DrawUtils.screenBorder(g, sx, sy, w, h)

    if (screen.getPowerState) {
      for (y <- 0 until screenHeight) {
        for (x <- 0 until screenWidth) {
          if (x == 0 || g.font.charWidth(screen.data.buffer(y)(x - 1)) != 16) {
            val char = screen.data.buffer(y)(x)
            val color = screen.data.color(y)(x)
            val bg = PackedColor.unpackBackground(color, screen.data.format)
            val fg = PackedColor.unpackForeground(color, screen.data.format)
            g.background = IntColor(bg)
            g.foreground = IntColor(fg)
            g.char(sx + x * fontWidth, sy + y * fontHeight, char)
          }
        }
      }
    } else {
      g.rect(sx, sy, w, h, ColorScheme("ScreenOff"))
    }

    g.setSmallFont()
    g.background = RGBAColor(0, 0, 0, 0)
    g.foreground = RGBAColor(110, 110, 110)

    val freeSpace = ((w - 15) / 8).toInt
    val addr = screen.node.address
    val text = if (addr.length <= freeSpace)
      addr
    else
      StringUtils.substring(addr, 0, (freeSpace - 1).max(0).min(addr.length)) + "…"

    g.text(sx - 4, sy - 14, text)
    g.setNormalFont()

    g.sprite("window/CloseButton", sx + w - 7, sy - 13, 7, 6)

    endDraw(g)
  }
}
