package ocelot.desktop.node.nodes

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.color.Color
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.node.Node
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry, ContextMenuSubmenu}
import ocelot.desktop.util.TierColor
import totoro.ocelot.brain.entity.traits.{Environment, GenericGPU}
import totoro.ocelot.brain.entity.{Keyboard, Screen}
import totoro.ocelot.brain.util.Tier

class ScreenNode(var screen: Screen) extends Node {
  OcelotDesktop.workspace.add(screen)

  private val keyboard = new Keyboard
  OcelotDesktop.workspace.add(keyboard)

  screen.connect(keyboard)

  override def environment: Environment = screen

  override def icon: String = "nodes/Screen"

  override def iconColor: Color = TierColor.get(screen.tier)

  override def setupContextMenu(menu: ContextMenu): Unit = {
    if (screen.getPowerState)
      menu.addEntry(new ContextMenuEntry("Turn off", () => screen.setPowerState(false)))
    else
      menu.addEntry(new ContextMenuEntry("Turn on", () => screen.setPowerState(true)))

    menu.addEntry(new ContextMenuSubmenu("Set tier") {
      addEntry(new ContextMenuEntry("Tier 1", () => changeTier(Tier.One)))
      addEntry(new ContextMenuEntry("Tier 2", () => changeTier(Tier.Two)))
      addEntry(new ContextMenuEntry("Tier 3", () => changeTier(Tier.Three)))
    })

    menu.addSeparator()
    super.setupContextMenu(menu)
  }

  private def changeTier(newTier: Int): Unit = {
    if (newTier == screen.tier) return

    val connections = screen.node.neighbors.toArray

    val gpus = screen.node.network.nodes
      .filter(_.host.isInstanceOf[GenericGPU])
      .map(_.host.asInstanceOf[GenericGPU]).toArray
      .filter(_.isBoundTo(screen.node.address))

    connections.foreach(screen.disconnect)
    OcelotDesktop.workspace.remove(screen)

    val newScreen = new Screen(newTier)
    OcelotDesktop.workspace.add(newScreen)
    newScreen.node.network.remap(newScreen.node, screen.node.address)

    screen = newScreen
    connections.foreach(screen.connect)
    gpus.foreach(_.forceBind(screen.node, reset = true))
  }

  override def draw(g: Graphics): Unit = {
    super.draw(g)

    if (screen.getPowerState)
      g.sprite("nodes/ScreenOnOverlay", position.x + 2, position.y + 2, size.width - 4, size.height - 4)
  }

  override lazy val window: Option[ScreenWindow] = Some(new ScreenWindow(this))
}
