package ocelot.desktop.node.nodes

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.{Padding2D, Size2D, Vector2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.layout.{Layout, LinearLayout}
import ocelot.desktop.ui.widget._
import ocelot.desktop.ui.widget.window.BasicWindow
import ocelot.desktop.util.{DrawUtils, Orientation}
import totoro.ocelot.brain.entity.Case
import totoro.ocelot.brain.util.Tier

class ComputerWindow(computer: Case) extends BasicWindow {
  private val slots = getSlots

  private val inner = new Widget {
    override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)

    children :+= new PaddingBox(new Label {
      override def text: String = computer.node.address
      override def isSmall: Boolean = true
      override def color: Color = ColorScheme("ComputerAddress")
    }, Padding2D(bottom = 8))

    children :+= new Widget {
      children :+= new PaddingBox(new Widget {
        children :+= new PaddingBox(new SlotWidget {
          override def icon: String = "icons/EEPROM"
        }, Padding2D(right = 10))

        children :+= new IconButton("buttons/PowerOff", "buttons/PowerOn",
          isSwitch = true, sizeMultiplier = 2)
        {
          override def isOn: Boolean = computer.machine.isRunning
          override def onPressed(): Unit = computer.turnOn()
          override def onReleased(): Unit = computer.turnOff()
        }
      }, Padding2D(top = 44, left = 40))

      children :+= new Widget {
        override protected val layout = new Layout(this)

        for (row <- slots) {
          children :+= new Widget {
            override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
            for (slot <- row) children :+= slot
          }
        }

        override def minimumSize: Size2D = Size2D(158, 140)

        override def draw(g: Graphics): Unit = {
          children(0).position = position + Vector2D(19, 8)
          children(1).position = position + Vector2D(63, 8)
          children(2).position = position + Vector2D(107, 8)

          g.sprite("ComputerMotherboard", bounds.mapX(_ - 4).mapW(_ - 4))
          drawChildren(g)
        }
      }
    }
  }

  children :+= new PaddingBox(inner, Padding2D(10, 12, 10, 12))

  private def getSlots: Array[Array[SlotWidget]] = {
    def cardSlot(tier: Int): SlotWidget = new SlotWidget {
      override def icon: String = "icons/Card"
      override def tierIcon: String = "icons/Tier" + tier
    }

    def cpuSlot(tier: Int): SlotWidget = new SlotWidget {
      override def icon: String = "icons/CPU"
      override def tierIcon: String = "icons/Tier" + tier
    }

    def memorySlot(tier: Int): SlotWidget = new SlotWidget {
      override def icon: String = "icons/Memory"
      override def tierIcon: String = "icons/Tier" + tier
    }

    def diskSlot(tier: Int): SlotWidget = new SlotWidget {
      override def icon: String = "icons/HDD"
      override def tierIcon: String = "icons/Tier" + tier
    }

    def floppySlot: SlotWidget = new SlotWidget {
      override def icon: String = "icons/Floppy"
      override def tierIcon: String = "icons/Tier0"
    }

    computer.tier match {
      case Tier.One => Array(
        Array(cardSlot(Tier.One), cardSlot(Tier.One)),
        Array(cpuSlot(Tier.One), memorySlot(Tier.One), memorySlot(Tier.One)),
        Array(diskSlot(Tier.One)))
      case Tier.Two => Array(
        Array(cardSlot(Tier.Two), cardSlot(Tier.One)),
        Array(cpuSlot(Tier.Two), memorySlot(Tier.Two), memorySlot(Tier.Two)),
        Array(diskSlot(Tier.Two), diskSlot(Tier.One)))
      case Tier.Three => Array(
        Array(cardSlot(Tier.Three), cardSlot(Tier.Two), cardSlot(Tier.Two)),
        Array(cpuSlot(Tier.Three), memorySlot(Tier.Three), memorySlot(Tier.Three)),
        Array(diskSlot(Tier.Three), diskSlot(Tier.Two), floppySlot))
      case _ => Array(
        Array(cardSlot(Tier.Three), cardSlot(Tier.Three), cardSlot(Tier.Three)),
        Array(cpuSlot(Tier.Three), memorySlot(Tier.Three), memorySlot(Tier.Three)),
        Array(diskSlot(Tier.Three), diskSlot(Tier.Three), floppySlot))
    }
  }

  override def draw(g: Graphics): Unit = {
    beginDraw(g)
    DrawUtils.windowWithShadow(g, position.x, position.y, size.width, size.height, 1f, 0.5f)
    drawChildren(g)
    endDraw(g)
  }
}