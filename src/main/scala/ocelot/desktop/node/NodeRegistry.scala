package ocelot.desktop.node

import ocelot.desktop.node.nodes.{ComputerNode, RelayNode, ScreenNode}
import totoro.ocelot.brain.entity.{Case, Relay, Screen}

import scala.collection.mutable

object NodeRegistry {
  val types: mutable.SortedSet[NodeType] = mutable.SortedSet[NodeType]()

  def register(t: NodeType): Unit = {
    types += t
  }

  register(NodeType("Relay", "nodes/Relay", -1, () => {
    new RelayNode(new Relay)
  }))

  for (tier <- 0 to 2) {
    register(NodeType("Screen" + tier, "nodes/Screen", tier, () => {
      new ScreenNode(new Screen(tier))
    }))
  }

  for (tier <- 0 to 3) {
    register(NodeType("Computer" + tier, "nodes/Computer", tier, () => {
      new ComputerNode(new Case(tier))
    }))
  }
}
