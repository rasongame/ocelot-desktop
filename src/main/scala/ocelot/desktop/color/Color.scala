package ocelot.desktop.color

object Color {
  var White: RGBAColorNorm = RGBAColorNorm(1, 1, 1)
  var Transparent: RGBAColorNorm = RGBAColorNorm(0, 0, 0, 0)
}

trait Color {
  def toInt: IntColor

  def toRGBA: RGBAColor

  def toRGBANorm: RGBAColorNorm

  def toHSVA: HSVAColor
}
