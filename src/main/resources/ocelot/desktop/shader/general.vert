#version 120

attribute vec2 inPos;
attribute vec2 inUV;

attribute vec4 inColor;
attribute vec4 inTextColor;
attribute vec3 inTransform0;
attribute vec3 inTransform1;
attribute vec3 inUVTransform0;
attribute vec3 inUVTransform1;
attribute vec3 inTextUVTransform0;
attribute vec3 inTextUVTransform1;

varying vec4 fColor;
varying vec2 fUV;
varying vec4 fTextColor;
varying vec2 fTextUV;

uniform mat3 uProj;

void main() {
    fColor = inColor;
    fTextColor = inTextColor;

    mat3 modelMatrix = transpose(mat3(inTransform0, inTransform1, vec3(0.0, 0.0, 1.0)));
    vec3 transformed = uProj * modelMatrix  * vec3(inPos, 1.0);

    gl_Position = vec4(transformed.xy, 1.0, 1.0);

    mat3 uvMatrix = transpose(mat3(inUVTransform0, inUVTransform1, vec3(0.0, 0.0, 1.0)));
    fUV = (uvMatrix * vec3(inUV, 1.0)).xy;

    uvMatrix = transpose(mat3(inTextUVTransform0, inTextUVTransform1, vec3(0.0, 0.0, 1.0)));
    fTextUV = (uvMatrix * vec3(inUV, 1.0)).xy;
}